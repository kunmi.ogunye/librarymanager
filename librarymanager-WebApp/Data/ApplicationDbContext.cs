﻿using librarymanager_WebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace librarymanager_WebApp.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) :base(options)
        {

        }

        public DbSet<User>? Users { get; set; }
    }
}
